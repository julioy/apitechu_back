var express = require('express');
var user_file = require('./user.json');
var bodyParser = require('body-parser');
const global = require('./global.js');
const user_controller = require('./controllers/user_controller.js');
const audit_controller = require('./controllers/audit_controller.js');
//
var app = express();
var port = process.env.port || 3000;
// //const URL_BASE = '/techu-peru/v1/'; //reemplazamos por global

app.use(bodyParser.json());

app.get(global.URL_BASE + 'users', user_controller.getUsers);
app.get(global.URL_BASE + 'users/:id', user_controller.getUsers);
app.post(global.URL_BASE + 'users', user_controller.postUsers);
app.put(global.URL_BASE + 'users/:id', user_controller.putUsers);
app.delete(global.URL_BASE + 'users/:id', user_controller.deleteUsers);
app.post(global.URL_BASE + 'login', audit_controller.login);
app.post(global.URL_BASE + 'logout/:id', audit_controller.logout);
// app.get(global.URL_BASE + 'userslab', user_controller.getUserslab);
// app.get(global.URL_BASE + 'userslab/:id', user_controller.getUserslab);
// //Peticion get de users
// app.get(global.URL_BASE + 'users',
//   function (req, res) {// recordar que esto es una funcion callback una funcion dentro de otra funcion
//    //res.send({"msg" : "generacion get exitosa"});
//    res.status(202).send(user_file);
//    //res.send(user_file);
// });
//
// //Peticion get de user con ID
// app.get(global.URL_BASE + 'users/:id',
//   function(req, res) {
//     let pos = req.params.id - 1;
//     console.log("GET con id = " + req.params.id);
//     let tam = user_file.length;
//     console.log(tam);
//     let respuesta = (user_file[pos] == undefined ) ?
//     {"msg": "Uusario No encontrado"} : user_file[pos];
//      res.send(respuesta);
// //  console.log("GET con id = " + req.params.otro);
// //  console.log("GET con id = " + req.params.pepito);
// });
//
// //Get Users con Query String
// app.get (global.URL_BASE + 'users',
// function(req, res){
//   console.log("Get con query String");
//   console.log(req.query.id);
//   console.log(req.query.name);
// });
//
// //Post Users
// app.post(global.URL_BASE + 'users',
//   function(req, res){
//     console.log('POST de users');
//       // res.send('Nuevo Usuario' + req.body);
//       // res.send('Nuevo Usuario' + req.body.first_name);
//       // res.send({"msg":"Post Exitoso"})
//   let newID = user_file.length + 1;
//   let newUser = {
//     "id" : newID,
//     "first_name" : req.body.first_name,
//     "last_name"  : req.body.last_name,
//     "email"  : req.body.email,
//     "password"  : req.body.password
//   }
//   user_file.push(newUser);
//   console.log("Nuevo Usuario :" + newUser);
//   res.send(newUser);
//   });
//
//   // PUT user actualizacion de usuario
//   // var id = parseInt(req.params.id);
//   // var updatedCustomer = req.body;
// app.put(global.URL_BASE + 'users/:id',
//    function(req, res){
//      console.log('PUT de user');
//      let pos = parseInt(req.params.id);
//      let udpID = {
//        "id" : req.params.id, //req.body.id,
//        "first_name" : req.body.first_name,
//        "last_name"  : req.body.last_name,
//        "email"  : req.body.email,
//        "password"  : req.body.password
//      }
//
//      let respuesta = (user_file[pos] == undefined ) ?
//      {"msg": "Uusario No encontrado"} : user_file[pos];
//
//      user_file.push(udpID);
//     // user_file  aca puso otra linea en vez de la de arriba
//      console.log("Usuario Modificado :" + udpID);
//      res.send(udpID);
//    }
// );
//
// app.delete(global.URL_BASE + 'users/:id',
//    function(req, res) {
//      console.log('Delete Usuarios');
//      let pos = req.params.id;
//      user_file.splice(pos - 1, 1);
//      res.send({"msg": "Usuario Eliminado"});
//    });
//
//    //Login
// app.post(global.URL_BASE + 'login',
//       function(req, res) {
//          console.log('Login');
//          console.log(req.body.email);
//          console.log(req.body.password);
//          let email = req.body.email;
//          let password = req.body.password;
//          let tam = user_file.length;
//          let i = 0;
//          let encontrado = false;
//          while (( i < user_file.length) && !encontrado){
//              if(user_file[i].email == email && user_file[i].password == password)
//              {
//               encontrado = true;
//               user_file[i].logged = true;
//               d(user_file);
//              }
//            i++;
//          }
//          if (encontrado)
//            res.send({"encontrado si id":"i","id usuario " : i})
//         else{
//           res.send({"encontrado no" : "no" })
//         }
//       });
//
// // Logout salir de la aplicacion o de la session.
// app.post(global.URL_BASE + 'logout/:id',
//       function(req, res) {
//          console.log('Logout');
//          console.log(req.body.email);
//          console.log(req.body.password);
//          let email = req.body.email;
//          let password = req.body.password;
//          let id = req.params.id;
//          let i = 0;
//          let encontrado = false;
//          while (( i < user_file.length) && !encontrado){
//              if(user_file[i].id == id)
//              {
//              encontrado = true;
//              delete user_file[i].logged ;
//              }
//            i++;
//          }
//
//          if (encontrado)
//            res.send({"encontrado id":"i","logged usuario eliminado " : id})
//          else{
//            res.send({"id usuario" : "no encontrado"})
//         }
//       });
//
// //Funcion que escribe en el archivo
//        function writeUserDataToFile(data) {
//        var fs = require('fs');
//        var jsonUserData = JSON.stringify(data);
//        fs.writeFile("./users.json", jsonUserData, "utf8",
//         function(err) { //función manejadora para gestionar errores de escritura
//           if(err) {
//             console.log(err);
//           } else {
//             console.log("Datos escritos en 'users.json'.");
//           }
//         })
//      }

app.listen(port, function(){
 console.log('Ejemplo de listen puerto 3000');
});
