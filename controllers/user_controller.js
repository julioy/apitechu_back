var express = require('express');
var user_file = require('../user.json');
var bodyParser = require('body-parser');
var app = express();
// var requestJSON = required('request-json');
// var baseMlabURL  = 'https://api.mlab.com/api/1/databases/techu45db/collections/';
// const apikeyMLab = "NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF";

//var port = process.env.port || 3000;
//Peticion get de users
function getUsers(req, res){
   res.status(200).send(user_file);
};

//Peticion get de user con IDeee
// function getUsers(req, res){
//    let pos = req.params.id - 1;
//    console.log("GET con id = " + req.params.id);
//    let tam = user_file.length;
//    console.log(tam);
//    let respuesta = (user_file[pos] == undefined ) ?
//    {"msg": "Uusario No encontrado"} : user_file[pos];
//    res.send(respuesta);
// };

//Get Users con Query String se creo getUsersS solo para que no se confunda con la
//funcion ya creda.
// function getUsers(req, res){
//   console.log("Get con query String");
//   console.log(req.query.id);
//   console.log(req.query.name);
//   res.send(user_file[pos -1 ]);
// };

//Post Users
function postUsers(req, res){
  console.log('POST de users');
      // res.send('Nuevo Usuario' + req.body);
      // res.send('Nuevo Usuario' + req.body.first_name);
      // res.send({"msg":"Post Exitoso"})
  let newID = user_file.length + 1;
  let newUser = {
    "id" : newID,
    "first_name" : req.body.first_name,
    "last_name"  : req.body.last_name,
    "email"      : req.body.email,
    "password"   : req.body.password
  }
  user_file.push(newUser);
  writeUserDataToFile(user_file);
  console.log("Nuevo Usuario :" + newUser);
  res.status(200);
  res.send({"mensaje":"Usuario creado con èxito.","userID":newUser});
};

// PUT user actualizacion de usuario
// app.put(global.URL_BASE + 'users/:id',
function putUsers(req, res){
  console.log('PUT de user');
  let pos = req.params.id - 1;
  let udpID = {
     "id" : req.params.id, //req.body.id,
     "first_name" : req.body.first_name,
     "last_name"  : req.body.last_name,
     "email"      : req.body.email,
     "password"   : req.body.password
  }
  // let respuesta = (user_file[pos] == undefined ) ?
  //  {"msg": "Uusario No encontrado"} : user_file[pos];
  if (user_file[pos] != undefined){
  //    user_file.push(udpID
      user_file[pos].first_name = req.body.first_name;
      user_file[pos].last_name = req.body.last_name;
      user_file[pos].email = req.body.email;
      user_file[pos].password = req.body.password;
      console.log("Usuario Modificado :" + udpID);
      writeUserDataToFile(user_file);
      res.status(201);
      res.send({"mensaje" : "Usuario actualizado con exito.",
      "lstUsuarios" : user_file});
  }else{
      res.send({"mensaje" : "Usuario no encontrado"});
  }
};

//Elimina Uusario
function deleteUsers(req, res) {
   console.log('Delete Usuarios');
   let pos = req.params.id;
   if(user_file[post-1] != undefined){
     user_file.splice(pos - 1, 1);
     res.send({"msg": "Usuario Eliminadocon exito","usuario":user_file});
   }
   else{
     res.send({"mensaje":"Recurso no encontrado."});
   }
};

// //La funcion Login es un Post
// function login(req, res) {
//    console.log('Login');
//    console.log(req.body.email);
//    console.log(req.body.password);
//    let email = req.body.email;
//    let password = req.body.password;
//    let tam = user_file.length;
//    let i = 0;
//    let encontrado = false;
//    while (( i < user_file.length) && !encontrado){
//        if(user_file[i].email == email && user_file[i].password == password)
//        {
//         encontrado = true;
//         user_file[i].logged = true;
//         //  d(user_file);
//        }
//    i++;
//    }
//    if (encontrado)
//      res.send({"encontrado si id":"i","id usuario " : i})
//    else{
//         res.send({"encontrado no" : "no" })
//    }
// };

// La funcion Logout salir de la aplicacion o de la session tambbien es un Post.
// function logout(req, res) {
//    console.log('Logout');
//    console.log(req.body.email);
//    console.log(req.body.password);
//    let email = req.body.email;
//    let password = req.body.password;
//    let id = req.params.id;
//    let i = 0;
//    let encontrado = false;
//    while (( i < user_file.length) && !encontrado){
//        if(user_file[i].id == id)
//        {
//          encontrado = true;
//          delete user_file[i].logged ;
//        }
//    i++;
//    }
//    if (encontrado)
//       res.send({"encontrado id":"i","logged usuario eliminado " : id})
//    else{
//       res.send({"id usuario" : "no encontrado"})
//    }
//  };

//Funcion que escribe en el archivo json, lo demas ha sido en memoria.
function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
         console.log(err);
      } else {
         console.log("Datos escritos en 'users.json'.");
      }
   })
};

// GET users consumiendo API REST de mLab
//app.get(URLbase + 'users',
//  function getUserslab(req, res) {
//    console.log("GET /colapi/v3/users");
//    var httpClient = requestJSON.createClient(baseMLabURL);
//    console.log("Cliente HTTP mLab creado.");
//    var queryString = 'f={"_id":0}&';
//    httpClient.get('user?' + queryString + apikeyMLab,
//      function(err, respuestaMLab, body) {
//        var response = {};
//        if(err) {
//            response = {"msg" : "Error obteniendo usuario."}
//            res.status(200);
//        } else {
//          if(body.length > 0) {
//            console.log(body.length);
//            response = body;
//          } else {
//            response = {"msg" : "Ningún elemento 'user'."}
//            res.status(404);
//          }
//        }
//        res.send(response);
//      });
// });

// GET users ID consumiendo API REST de mLab
// app.get(URLbase + 'users',
//  function(req, res) {
//    console.log("GET /colapi/v3/users");
//    var httpClient = requestJSON.createClient(baseMLabURL);
//    console.log("Cliente HTTP mLab creado.");
//    var queryString = 'f={"_id":0}&';
//    httpClient.get('user?' + queryString + apikeyMLab,
//      function(err, respuestaMLab, body) {
//        var response = {};
//        if(err) {
//            response = {"msg" : "Error obteniendo usuario."}
//            res.status(200);
//        } else {
//          if(body.length > 0) {
//            console.log(body.length);
//            response = body;
//          } else {
//            response = {"msg" : "Ningún elemento 'user'."}
//            res.status(404);
//          }
//        }
//        res.send(response);
//      });
// });

// Esto solo es para exponer y otro pueda consumirlo
module.exports.getUsers = getUsers;
module.exports.postUsers = postUsers;
module.exports.putUsers = putUsers;
module.exports.deleteUsers = deleteUsers;
//module.exports.getUsers = getUserslab;
