var express = require('express');
var user_file = require('../user.json');
var bodyParser = require('body-parser');
var app = express();

//Login es un Post
function login(req, res) {
   console.log('Login');
   console.log(req.body.email);
   console.log(req.body.password);
   let email = req.body.email;
   let password = req.body.password;
   let tam = user_file.length;
   let i = 0;
   let encontrado = false;
   while (( i < user_file.length) && !encontrado){
       if(user_file[i].email == email && user_file[i].password == password)
       {
        encontrado = true;
        user_file[i].logged = true; // se agrega un logged cada vez que usa login
       }
   i++;
   }
   if (encontrado)
     res.send({"encontrado si id":"i","id usuario " : i})
   else{
     res.send({"encontrado no" : "no" })
   }
};

// Logout: Salir de la aplicacion o de la session es un Post.
function logout(req, res) {
   console.log('Logout');
   console.log(req.body.email);
   console.log(req.body.password);
   let email = req.body.email;
   let password = req.body.password;
   let id = req.params.id;
   let i = 0;
   let encontrado = false;
   while (( i < user_file.length) && !encontrado){
       if(user_file[i].id == id)
       {
         encontrado = true;
         delete user_file[i].logged ;
       }
   i++;
   }
   if (encontrado)
      res.send({"encontrado id":"i","logged usuario eliminado " : id})
   else{
      res.send({"id usuario" : "no encontrado"})
   }
};

// Exponemos las funciones para que puedan ser consumidas desde otra parte del proyecto.
module.exports.login = login;
module.exports.logout = logout;
